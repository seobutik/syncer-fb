import os

ELASTIC_URI = os.environ["ELASTIC_URL"]
AMQP_URI = os.environ["AMQP_URI"]