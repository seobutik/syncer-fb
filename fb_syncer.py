import json
import websockets
from celery import Celery
from elasticsearch import Elasticsearch
import asyncio
import settings.prod as settings


app = Celery('fb_syncer', broker=settings.AMQP_URI)
es = Elasticsearch(settings.ELASTIC_URI)


async def fetch(uri):
    async with websockets.connect(uri) as websocket:
        print("sending...")
        await websocket.send(json.dumps({"task": "get_subscribers"}))
        while True:
            try:
                data = await websocket.recv()
                process_item.apply_async([data])
            except websockets.ConnectionClosed:
                break


@app.task
def process_item(data):
    if data is not None:
        data = json.loads(data)
        sid = data['uid']
        data.pop('uid', None)
        persist_item.apply_async([data, sid])
    else:
        print('error')


@app.task
def persist_item(data, sid):
    if data is not None:
        es.index(index='users', doc_type='post', id=sid, body=data)
        print('inserted...')
    else:
        print('error')


@app.task
def load_data():
    print("started1...")
    asyncio.get_event_loop().run_until_complete(
        fetch('wss://ws-fb-push.news-host.pw'))


load_data.apply_async()
